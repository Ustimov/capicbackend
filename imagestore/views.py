from django.shortcuts import render
from . import models
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import os

# Create your views here.

def list(request):
    items = models.Image.objects.all()
    response = []
    for i in items:
        response.append({
            "text": i.text,
            "image": i.image.url,
        })
    return JsonResponse(response, safe=False)

@csrf_exempt
def upload(request):
    if request.method == "POST":
        file = request.FILES['file']
        if len(models.Image.objects.all()) > 4:
            return JsonResponse({"result": False})
        img = models.Image.objects.create(image=file, text=os.path.splitext(file.name)[0])
        img.save()
    return JsonResponse({"result": True})

@csrf_exempt
def delete(request):
    if request.method == "POST":
        images = models.Image.objects.filter(text=request.POST.get("name"))
        if images:
            images[0].delete()
    return JsonResponse({"result": True})